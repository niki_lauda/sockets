/*
######################################
## Sockets API 2
## Running client
## Student KP-32
## Maliavka Yevhenii
######################################
*/

#include <stdio.h> //I/O operations
#include <sys/socket.h> //sockets API
#include <arpa/inet.h> //inet_add
#include <stdlib.h> //malloc, atol
#include <string.h> //strlen
#include<netdb.h> //hostent
#include <unistd.h> //read, close


//expected 3 arguments: ip address of a remote, port, size of a single message
int main(int argc, char** argv){
  //help
  if(argc == 2 && strcmp(argv[1],"/?")){
    printf("###\n3 arguments are expected:ip address running port, size of a single message\n###\n");
    return 0;
  }

  //initial parameters set-up
  if(argc != 4){
      printf("Error! Invalid number of arguments!");
      return -1;
  }

  char* ipAddress = argv[1];
  int port = atol(argv[2]);
  int bufferSize = atol(argv[3]);

  char* message = (char*)malloc(bufferSize);
  char* reply = (char*)malloc(bufferSize);

  //trying to create the socket and get its descriptor
  //AF_NET - protocol family - IPv4 standard
  //SOCK_STREAM - kind of socket - TCP stream bytes
  //0 - protocol type - <TCP> <UDP> <SCTP> or <0> - for using default
  struct sockaddr_in serverAddr;
  int socketDesc = socket(AF_INET, SOCK_STREAM, 0);

  if(socketDesc == -1){
    printf("Error! Socket descriptor could not be created!\n");
    close(socketDesc);
    return -1;
  }
  printf("Success! Socket has been created\n");

  //inet_addr - converting string ip adress to long format
  serverAddr.sin_addr.s_addr = inet_addr(ipAddress);
  serverAddr.sin_family = AF_INET;
  //htons - converting 16-bit (2-byte) quantities from host byte order to
  //network byte order
  serverAddr.sin_port = htons(port);

  //CONNECTING TO REMOTE
  if(connect(socketDesc, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0){
    printf("Error! Connection could not be established!\n");
    close(socketDesc);
    return -1;
  }
  printf("Success! Connection has been established!\n");

  message = "FOK U";
  //SENDING MESSAGE TO REMOTE
  if(send(socketDesc, message, strlen(message), 0) < 0){
    printf("Error! Message could not be send!\n");
    close(socketDesc);
    return -1;
  }
  printf("Success! Request to %s has been sent!\n", "site");

  //READING FROM REMOTE
  int bytes = 0;
  int totalSize = 0;
  printf("######\n");
  while(bytes = recv(socketDesc, reply, bufferSize, 0)){
    printf("\n###########BUNCH OF DATA HAS BEEN READ!###########\n");
    printf("\n%s\n", reply);
    memset(reply, 0, bufferSize);
    totalSize += bytes;
    printf("Press any key to continue reading...\n");
    getchar();
  }
  printf("######\n");

  //TERMINATION
  printf("Success! Reply from %s has been received! %d bytes has been read!\n", ipAddress, totalSize);
  close(socketDesc);
  printf("Socket has been closed\n");
  return 0;
}
