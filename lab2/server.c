/*
######################################
## Sockets API 2
## Running server
## Student KP-32
## Maliavka Yevhenii
######################################
*/

#include <stdio.h> //I/O operations
#include <sys/socket.h> //sockets API
#include <arpa/inet.h> //inet_add
#include <stdlib.h> //malloc, atol
#include <string.h> //strlen
#include <netdb.h> //hostent
#include <unistd.h> //read, close
#include <stdio.h> //fread

//expects 2 arguments: port, size of a single message
int main(int argc, char** argv){
  //help
  if(argc == 2 && strcmp(argv[1],"/?")){
    printf("###\n2 arguments are expected:running port, size of a single message\n###\n");
    return 0;
  }

  //initial parameters set-up
  if(argc != 3){
      printf("Error! Invalid number of arguments!");
      return -1;
  }

  int port = atol(argv[1]);
  int bufferSize = atol(argv[2]);

  char* message = (char*)malloc(bufferSize);
  char* reply = (char*)malloc(bufferSize);

  int MAX_QUEUE_LEN = 3;
  char* DEFAULT_IP_ADDRESS = "127.0.0.1";

  //INITIATION
  //trying to create the socket and get its descriptor
  //AF_NET - protocol family - IPv4 standard
  //SOCK_STREAM - kind of socket - TCP stream bytes
  //0 - protocol type - <TCP> <UDP> <SCTP> or <0> - for using default
  struct sockaddr_in server, client;
  int socketDesc = socket(AF_INET, SOCK_STREAM, 0);
  int socketDescClient = 0;
  if(socketDesc == -1){
    printf("Error! Socket descriptor could not be created!\n");
    close(socketDesc);
    return -1;
  }
  printf("Success! Socket has been created\n");

  //inet_addr - converting string ip adress to long format
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_family = AF_INET;
  //htons - converting 16-bit (2-byte) quantities from host byte order to
  //network byte order
  server.sin_port = htons(port);

  //BINDING
  if(bind(socketDesc, (struct sockaddr*)&server, sizeof(server)) < 0){
    printf("Error! Could not bind!\n");
    close(socketDesc);
    return -1;
  }
  printf("Success! Bind has been done!\n");

  //LISTEN
  listen(socketDesc, MAX_QUEUE_LEN);
  printf("Server is listening on %s using port %d...\n", inet_ntoa(server.sin_addr), ntohs(server.sin_port));
  int structSize = sizeof(struct sockaddr_in);

  FILE* file;
  int totalSize = 0;
  while(socketDescClient = accept(socketDesc, (struct sockaddr*)&client, (socklen_t*)&structSize)){
    totalSize = 0;
    if(socketDescClient < 0){
      printf("Error! Accept failed!");
      return -1;
    }
    printf("Success! Connection from %s using port %d has been accepted!\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));

    file = fopen("sockets.txt", "r");
    if(!file){
      write(socketDescClient, message, strlen(message));
      sprintf(message, "Thanks for your connection, %s! See you soon!\n", inet_ntoa(client.sin_addr));
      memset(message, 0, bufferSize);
      shutdown(socketDescClient, SHUT_RDWR);
    }
    else{
      int nbytes = 0;

      while((nbytes = fread(message, 1, bufferSize, file)) > 0 ){
        write(socketDescClient, message, strlen(message));
        memset(message, 0, bufferSize);
        totalSize += nbytes;
      }
    }
    sprintf(message, "Thanks for your connection, %s! See you soon!\n", inet_ntoa(client.sin_addr));
    write(socketDescClient, message, strlen(message));
    memset(message, 0, bufferSize);
    shutdown(socketDescClient, SHUT_RDWR);
  }

  close(socketDesc);
  printf("Socket has been closed!\n");
  return 0;
}
