/*
######################################
## Sockets API 3
## Chat server
## Student KP-32
## Maliavka Yevhenii
######################################
*/

#include <stdio.h> //I/O operations
#include <sys/socket.h> //sockets API
#include <arpa/inet.h> //inet_add
#include <stdlib.h> //malloc, atol
#include <string.h> //strlen
#include <netdb.h> //hostent
#include <unistd.h> //read, close
#include <stdio.h> //fread
#include <pthread.h> //threading

int paramsCheck(int argc, char** argv){
  //help
  if(argc == 2 && !strcmp(argv[1],"/?")){
    printf("###\n2 arguments are expected:running port, size of a single message\n###\n");
    return -1;
  }
  //initial parameters set-up
  if(argc != 2){
      printf("Error! Invalid number of arguments!");
      return -1;
  }
  return 0;
}

void safeClose(int socketDesc, char* msg){
  printf("%s!\n", msg);
  close(socketDesc);
}

void* writeMessage(void* arg);
void* readMessage(void* arg);


//expects 1 arguments: port
int main(int argc, char** argv){

  if(paramsCheck(argc, argv)){
    return -1;
  }
  int socketDesc = 0;
  int port = atol(argv[1]);
  int bufferSize = 2000;
  char* message = (char*)malloc(bufferSize);
  char* passphase = (char*)malloc(bufferSize);
  char* reply = (char*)malloc(bufferSize);
  int socketDescClient = 0, MAX_QUEUE_LEN = 3;
  struct sockaddr_in server, client;

  if((socketDesc = socket(AF_INET, SOCK_STREAM, 0)) == -1){
    safeClose(socketDesc, "Error! Socket descriptor could not be created!\n");
    return -1;
  }

  printf("Success! Socket has been created\n");

  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_family = AF_INET;
  server.sin_port = htons(port);

  if(bind(socketDesc, (struct sockaddr*)&server, sizeof(server)) < 0){
      safeClose(socketDesc, "Error! Could not bind!");
      return -1;
  }

  printf("Success! Bind has been done!\n");
  listen(socketDesc, MAX_QUEUE_LEN);
  printf("Server is listening on %s using port %d...\n", inet_ntoa(server.sin_addr), ntohs(server.sin_port));
  int structSize = sizeof(struct sockaddr_in);

  socketDescClient = accept(socketDesc, (struct sockaddr*)&client, (socklen_t*)&structSize);
/////////////////////////////////////////////////////////////
/////BLOCKED UNTIL A CONNECTION
/////////////////////////////////////////////////////////////
  if(socketDescClient < 0){
    safeClose(socketDesc, "Error! Accept failed!");
    return -1;
  }
  printf("Connection to %s using port %d has been established!\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));

/////////////////////////////////////////////////////////////
/////CHATTING
/////////////////////////////////////////////////////////////
  short access = 0;
  printf("Enter a passphase please: ");
  scanf("%s", passphase);
  while(!access){
    memset(message, 0, bufferSize);
    sprintf(message, "%s!\nEnter a passphase please: ", inet_ntoa(client.sin_addr));
    write(socketDescClient, message, strlen(message));
    memset(reply, 0, bufferSize);
    recv(socketDescClient, reply, bufferSize, 0);
    printf("Client: %s\n", reply);
    if(!strcmp(reply, passphase)){
      access = 1;
      memset(message, 0, bufferSize);
      sprintf(message, "granted");
      write(socketDescClient, message, strlen(message));
    }
  }

  //////////////////CHATTING HERE
  pthread_t threadWrite, threadRead;

  int threadW, threadR;
  fflush(stdin);
  threadW = pthread_create(&threadWrite, NULL, writeMessage, (void*)&socketDescClient);
  if(threadW){
    printf("ERROR THREAD!");
  }
  threadR= pthread_create(&threadRead, NULL, readMessage, (void*)&socketDescClient);
  if(threadR){
    printf("ERROR THREAD!");
  }

  pthread_join(threadW, NULL);
  pthread_join(threadR, NULL);

  pthread_exit(0);
  close(socketDesc);
  printf("Socket has been closed!\n");
  return 0;
}

void* writeMessage(void* arg){
  int bufferSize = 2000;
    int read_size;
    char* message = (char*)malloc(2000);
    int socketDesc = *(int*)arg;
    while(1){
        memset(message, 0, 2000);
        fflush(stdin);
        fgets(message, bufferSize, stdin);
        write(socketDesc, message, bufferSize);
    }
}

void* readMessage(void* arg){
  int read_size;
  char* reply = (char*)malloc(2000);
  int socketDesc = *(int*)arg;
  while(1){
    if(recv(socketDesc, reply, 2000, 0)){
      printf("Client: %s", reply);
      memset(reply, 0, 2000);
    }
  }
}
