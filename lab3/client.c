/*
######################################
## Sockets API 3
## Chat client
## Student KP-32
## Maliavka Yevhenii
######################################
*/

#include <stdio.h> //I/O operations
#include <sys/socket.h> //sockets API
#include <arpa/inet.h> //inet_add
#include <stdlib.h> //malloc, atol
#include <string.h> //strlen
#include<netdb.h> //hostent
#include <unistd.h> //read, close
#include <pthread.h> //threading


void* writeMessage(void* arg);
void* readMessage(void* arg);


//expected 2 arguments: ip address of a remote, port
int main(int argc, char** argv){
  //help
  if(argc == 2 && strcmp(argv[1],"/?")){
    printf("###\n2 arguments are expected:running port, size of a single message\n###\n");
    return 0;
  }

  //initial parameters set-up
  if(argc != 3){
      printf("Error! Invalid number of arguments!");
      return -1;
  }

  char* ipAddress = argv[1];
  int port = atol(argv[2]);
  int bufferSize = 2000;

  char* message = (char*)malloc(bufferSize);
  char* reply = (char*)malloc(bufferSize);

  //trying to create the socket and get its descriptor
  //AF_NET - protocol family - IPv4 standard
  //SOCK_STREAM - kind of socket - TCP stream bytes
  //0 - protocol type - <TCP> <UDP> <SCTP> or <0> - for using default
  struct sockaddr_in serverAddr;
  int socketDesc = socket(AF_INET, SOCK_STREAM, 0);

  if(socketDesc == -1){
    printf("Error! Socket descriptor could not be created!\n");
    close(socketDesc);
    return -1;
  }
  printf("Success! Socket has been created\n");

  //inet_addr - converting string ip adress to long format
  serverAddr.sin_addr.s_addr = inet_addr(ipAddress);
  serverAddr.sin_family = AF_INET;
  //htons - converting 16-bit (2-byte) quantities from host byte order to
  //network byte order
  serverAddr.sin_port = htons(port);

  //CONNECTING TO REMOTE
  if(connect(socketDesc, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0){
    printf("Error! Connection could not be established!\n");
    close(socketDesc);
    return -1;
  }
  printf("Successss! Connection has been established!\n");

  //SENDING MESSAGE TO REMOTE
  // if(send(socketDesc, message, strlen(message), 0) < 0){
  //   printf("Error! Message could not be send!\n");
  //   close(socketDesc);
  //   return -1;
  // }
  // printf("Success! Request to %s has been sent!\n", "site");
  //
  //READING FROM REMOTE
  recv(socketDesc, reply, bufferSize, 0);
  printf("Server: %s\n", reply);

  short access = 0;
  while(!access){
    memset(message, 0, bufferSize);
    scanf("%s", message);
    write(socketDesc, message, bufferSize);
    memset(reply, 0, bufferSize);
    recv(socketDesc, reply, bufferSize, 0);
    printf("Server: %s\n", reply);
    if(!strcmp(reply, "granted")){
      access = 1;
    }
  }

  ///////////////////////CHATTING HERE
  pthread_t threadWrite, threadRead;

  int threadW, threadR;
  fflush(stdin);
  threadW = pthread_create(&threadWrite, NULL, writeMessage, (void*)&socketDesc);
  if(threadW){
    printf("ERROR THREAD!");
  }
  threadR= pthread_create(&threadRead, NULL, readMessage, (void*)&socketDesc);
  if(threadR){
    printf("ERROR THREAD!");
  }

  pthread_join(threadW, NULL);
  pthread_join(threadR, NULL);

  pthread_exit(0);
  close(socketDesc);
  printf("Socket has been closed\n");
  return 0;
}

void* writeMessage(void* arg){
  int bufferSize = 2000;
    int read_size;
    char* message = (char*)malloc(2000);
    int socketDesc = *(int*)arg;
    while(1){
        memset(message, 0, 2000);
        fflush(stdin);
        fgets(message, bufferSize, stdin);
        write(socketDesc, message, bufferSize);
    }
}

void* readMessage(void* arg){
  int read_size;
  char* reply = (char*)malloc(2000);
  int socketDesc = *(int*)arg;
  while(1){
    if(recv(socketDesc, reply, 2000, 0)){
      printf("Server: %s", reply);
      memset(reply, 0, 2000);
    }
  }
}
