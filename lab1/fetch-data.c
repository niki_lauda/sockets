/*
######################################
## Sockets API 1
## Getting Data from Remote
## Student KP-32
## Maliavka Yevhenii
######################################
*/

#include <stdio.h> //I/O operations
#include <sys/socket.h> //sockets API
#include <arpa/inet.h> //inet_add
#include <stdlib.h> //malloc, atol
#include <string.h> //strlen
#include<netdb.h> //hostent
#include <unistd.h> //read, close

//details http://beej.us/guide/bgnet/output/html/multipage/gethostbynameman.html
int hostnameToIp(char* hostName, char* ip){
  struct hostent *he;
  struct in_addr **addr_list;
  int i = 0;

  he = gethostbyname(hostName);

  if(he == NULL){
    printf("Error! Host name could not be resolved!\n");
    return -1;
  }
  addr_list = (struct in_addr**) he->h_addr_list;
  for(i = 0; addr_list[i] != NULL; i++){
    strcpy(ip, inet_ntoa(*addr_list[i]));
    printf("Success! Host name %s (official name %s) has been resolved to %s\n", hostName, he->h_name, ip);
    return 0;
  }
}

//expected 4 arguments: hostname, port, relative url, buffer size
int main(int argc, char** argv){
  //help
  if(argc == 2 && strcmp(argv[1],"/?")){
    printf("###\n4 arguments are expected: hostname, port, relative url or route, buffer size for reply\n###\n");
    return 0;
  }

  //initial parameters set-up
  if(argc != 5){
      printf("Error! Invalid number of arguments!");
      return -1;
  }

  //INITIATION
  char* hostname = argv[1];
  int port = atol(argv[2]);
  char* route = argv[3];
  long bufferSize = atol(argv[4]);
  //trying to create the socket and get its descriptor
  //AF_NET - protocol family - IPv4 standard
  //SOCK_STREAM - kind of socket - TCP stream bytes
  //0 - protocol type - <TCP> <UDP> <SCTP> or <0> - for using default
  struct sockaddr_in server;
  char message[256];
  char ipAddress[256];
  int socketDesc = socket(AF_INET, SOCK_STREAM, 0);
  char* reply = (char*)malloc(bufferSize);

  if(socketDesc == -1){
    printf("Error! Socket descriptor could not be created!\n");
    close(socketDesc);
    return -1;
  }
  printf("Success! Socket has been created\n");

  hostnameToIp(hostname, ipAddress);

  //inet_addr - converting string ip adress to long format
  server.sin_addr.s_addr = inet_addr(ipAddress);
  server.sin_family = AF_INET;
  //htons - converting 16-bit (2-byte) quantities from host byte order to
  //network byte order
  server.sin_port = htons(port);

  //CONNECTING TO REMOTE
  if(connect(socketDesc, (struct sockaddr *)&server, sizeof(server)) < 0){
    printf("Error! Connection could not be established!\n");
    close(socketDesc);
    return -1;
  }
  printf("Success! Connection has been established!\n");

  //command to get the main page of the website
  sprintf(message, "GET http://%s%s HTTP/1.1\r\nHost: www.%s\r\n\r\n", hostname, route, hostname);
  printf("%s\n", message);

  //SENDING MESSAGE TO REMOTE
  if(send(socketDesc, message, strlen(message), 0) < 0){
    printf("Error! Message could not be send!\n");
    close(socketDesc);
    return -1;
  }
  printf("Success! Request to %s has been sent!\n", "site");

  //READING FROM REMOTE
  int bytes = 0;
  int totalSize = 0;
  printf("######\n");
  while(bytes = read(socketDesc, reply, bufferSize)){
    printf("\n###########BUNCH OF DATA HAS BEEN READ!###########\n");
    printf("\n%s\n", reply);
    memset(reply, 0, bufferSize);
    totalSize += bytes;
    printf("Press any key to continue reading...\n");
    getchar();
  }
  printf("######\n");

  //TERMINATION
  printf("Success! Reply from %s has been received! %d bytes has been read!\n", hostname, totalSize);
  close(socketDesc);
  printf("Socket has been closed\n");
  return 0;
}
